require 'rails_helper'

RSpec.describe "TournamentEntrants", type: :request do
  describe "GET /team_id:integer" do
    it "returns http success" do
      get "/tournament_entrants/team_id:integer"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /tournament_id:integer" do
    it "returns http success" do
      get "/tournament_entrants/tournament_id:integer"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /team_eliminated:boolean" do
    it "returns http success" do
      get "/tournament_entrants/team_eliminated:boolean"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /competition_type:integer" do
    it "returns http success" do
      get "/tournament_entrants/competition_type:integer"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /wins_count:integer" do
    it "returns http success" do
      get "/tournament_entrants/wins_count:integer"
      expect(response).to have_http_status(:success)
    end
  end

end
