class AddFinalistFieldToTournaments < ActiveRecord::Migration[6.1]
  def up
    add_column :tournaments, :finalist, :integer
  end

  def down
    remove_column :tournaments, :finalist, :integer
  end
end
