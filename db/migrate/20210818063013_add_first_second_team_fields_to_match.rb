class AddFirstSecondTeamFieldsToMatch < ActiveRecord::Migration[6.1]
  def up
    add_column :matches, :first_team, :integer
    add_column :matches, :second_team, :integer
    add_column :matches, :winner_id, :integer
    add_column :matches, :group, :integer

    remove_column :matches, :first_player_id, :integer
    remove_column :matches, :second_player_id, :integer
    remove_column :matches, :first_player_score, :integer
    remove_column :matches, :second_player_score, :integer
  end

  def down
    remove_column :matches, :first_team, :integer
    remove_column :matches, :second_team, :integer
    remove_column :matches, :winner_id, :integer
    remove_column :matches, :group, :integer

    add_column :matches, :first_player_id, :integer
    add_column :matches, :second_player_id, :integer
    add_column :matches, :first_player_score, :integer
    add_column :matches, :second_player_score, :integer
  end
end
