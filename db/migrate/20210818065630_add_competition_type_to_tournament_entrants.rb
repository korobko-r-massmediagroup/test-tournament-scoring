class AddCompetitionTypeToTournamentEntrants < ActiveRecord::Migration[6.1]
  def up
    add_column :tournament_entrants, :team_eliminated, :boolean, null: false, default: false
    add_column :tournament_entrants, :competition_type, :integer
    add_column :tournament_entrants, :wins_count, :integer
  end

  def down
    remove_column :tournament_entrants, :team_eliminated, :boolean, null: false, default: false
    remove_column :tournament_entrants, :competition_type, :integer
    remove_column :tournament_entrants, :wins_count, :integer
  end
end
