class CreateTournaments < ActiveRecord::Migration[6.1]
  def change
    create_table :tournaments do |t|
      t.string :name, null: false
      t.integer :status, default: 0, null: false


      t.timestamps
    end
  end
end
