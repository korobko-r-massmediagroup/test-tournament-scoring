class CreateMatches < ActiveRecord::Migration[6.1]
  def change
    create_table :matches do |t|
      t.integer :tournament_id
      t.integer :first_player_id
      t.integer :second_player_id
      t.integer :first_player_score
      t.integer :second_player_score

      t.timestamps
    end
  end
end
