class CreateTournamentEntrants < ActiveRecord::Migration[6.1]
  def change
    create_table :tournament_entrants do |t|
      t.integer :team_id
      t.integer :tournament_id

      t.timestamps
    end

  end
end
