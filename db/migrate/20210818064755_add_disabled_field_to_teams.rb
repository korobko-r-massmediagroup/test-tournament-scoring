class AddDisabledFieldToTeams < ActiveRecord::Migration[6.1]
  def up
    add_column :teams, :wins_count, :integer
  end

  def down
    remove_column :teams, :wins_count, :integer
  end
end
