class MatchesController < ApplicationController
  before_action :set_match, only: %i[show edit update destroy]
  before_action :set_tournament, only: %i[index show new create edit update generate_results_for_all_matches]

  def index
    @matches = @tournament.matches
  end

  def edit
  end

  def show
  end

  def new
    @match = Match.new
  end

  def create
    @match = Match.new(match_params)

    if @match.save
      redirect_to @match, notice: "Match was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @match.update(match_params)
      redirect_to tournament_match_url(@tournament, @match), notice: "Match was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def generate_results_for_all_matches
    @tournament.matches.each { |match| GroupMatchPlayService.new(match).call }
    redirect_to tournament_matches_path(@tournament)
  end

  private

  def set_match
    @match = Match.find(params[:id])
  end

  def set_tournament
    @tournament = Tournament.find(params[:tournament_id])
  end

  def match_params
    params.require(:match).permit(:tournament_id, :first_team, :second_team, :winner_id, :group)
  end
end