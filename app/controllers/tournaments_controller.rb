class TournamentsController < ApplicationController
  before_action :set_tournament, only: %i[ show edit update destroy generate_matches generate_playoff_matches]

  def index
    @tournaments = Tournament.all
  end

  def show
    @winners = @tournament.matches.where(winner_id: nil).exists?
  end

  def new
    @tournament = Tournament.new
  end

  def edit
  end

  def create
    @tournament = Tournament.new(tournament_params)

    result = Tournaments::SaveTournament.new(@tournament, params).perform if params.dig(:tournament, :team_ids).present?

    if (result.present? && result.success?) || @tournament.save
      redirect_to @tournament, notice: "Tournament was successfully created."
    else
      render :new
    end
  end

  def update
    if @tournament.update(tournament_params)
      redirect_to @tournament, notice: "Tournament was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @tournament.destroy
    redirect_to tournaments_url, notice: "Tournament was successfully destroyed."
  end

  def generate_matches
    MatchesGeneratorService.new(@tournament).call
    redirect_to tournament_matches_path(@tournament)
  end

  def generate_playoff_matches
    PlayoffMatchesGeneratorService.new(@tournament).call
    redirect_to tournaments_url
  end

  private
    def set_tournament
      @tournament = Tournament.find(params[:id])
    end

    def tournament_params
      params.require(:tournament).permit(:name, :status, :finalist)
    end
end
