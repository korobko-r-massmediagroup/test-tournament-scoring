class TournamentEntrantsController < ApplicationController
  before_action :set_tournament
  before_action :set_tournament_entrant, only: [:show, :edit, :update, :destroy]

  def index
    @tournament_entrants = @tournament.tournament_entrants
  end

  def show
  end

  def new
    @tournament_entrant = TournamentEntrant.new
  end

  def edit
  end

  def create
    @tournament_entrant = TournamentEntrant.new(tournament_entrant_params)

    if @tournament_entrant.save
      redirect_to @tournament, notice: "Tournament Entrant was successfully created."
    else
      render @tournament, status: :unprocessable_entity
    end
  end

  def update
    if @tournament_entrant.update(tournament_entrant_params)
      redirect_to @tournament_entrant, notice: "Tournament Entrant was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @tournament_entrant.destroy
    redirect_to tournament_tournament_entrants_path, notice: "Tournament Entrant was successfully destroyed."
  end

  private

  def set_tournament
    @tournament ||= Tournament.find(params[:tournament_id])
  end

  def set_tournament_entrant
    @tournament_entrant = TournamentEntrant.find(params[:id])
  end

  def tournament_entrant_params
    params.require(:tournament_entrant).permit(:team_id, :tournament_id, :team_eliminated, :competition_type)
  end
end
