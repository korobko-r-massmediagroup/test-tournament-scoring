class PlayoffMatchesGeneratorService < BaseService
  attr_reader :tournament

  def initialize(tournament)
    @tournament = tournament
  end

  def call
    playoff_teams_from_group_a = tournament.teams.joins(:tournament_entrants)
                                           .where(tournament_entrants: { competition_type: :group_a }).order(:wins_count).reverse.first(4)

    playoff_teams_from_group_b = tournament.teams.joins(:tournament_entrants)
                                           .where(tournament_entrants: { competition_type: :group_b }).order(:wins_count).reverse.first(4)

    playoff_team_ids = (playoff_teams_from_group_a + playoff_teams_from_group_b).pluck(:id)

    tournament.tournament_entrants.where(team_id: playoff_team_ids).each do |tournament_entrant|
      tournament_entrant.update!(competition_type: :playoff, wins_count: 0)
    end

    while Team.where(id: tournament.tournament_entrants.where(competition_type: :playoff, team_eliminated: false).pluck(:team_id)).count > 1 do
      play_best_vs_worst
    end

    finalist = Team.where(id: tournament.tournament_entrants.where(competition_type: :playoff, team_eliminated: false).pluck(:team_id)).first

    tournament.update!(finalist: finalist.id)
  end

  private

  def play_best_vs_worst
    teams = Team.where(id: tournament.tournament_entrants.where(competition_type: :playoff, team_eliminated: false)).order(:wins_count).reverse

    better_team = teams.first
    not_better_team = teams.last
    winner = winner(better_team, not_better_team)
    loser = winner == better_team ? not_better_team : better_team
    Match.create!(first_team: better_team, second_team: not_better_team, winner: winner, group: :playoff, tournament_id: tournament.id)
    tournament.tournament_entrants.where(team_id: loser).update!(team_eliminated: true)
  end

  def winner(first_team, second_team)
    [first_team, second_team].sample
  end
end