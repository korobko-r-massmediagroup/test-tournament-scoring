class BaseService
  def self.call(*args)
    new(*args).call
  end

  def initialize(*_args)
  end
end