class GroupMatchPlayService < BaseService
  attr_reader :match, :winner_id

  def initialize(match, winner_id = nil)
    @match = match
    @winner_id = winner_id
  end

  def call
    match.update!(winner_id: winner)
    Team.find(winner).increment!(:wins_count)
  end

  private

  def winner
    @winner ||= Team.find_by(id: winner_id) || [match.first_team, match.second_team].sample
  end
end