module Tournaments
  class SaveTournament
    Result = Struct.new(:success?, :error, :value)

    attr_reader :params, :tournament

    def initialize(tournament, params)
      @tournament = tournament
      @params = params
    end

    def perform
      if tournament.save
          team_ids = params.dig(:tournament, :team_ids)

          randomize_teams = team_ids.shuffle.each_slice(8).to_a

          randomize_teams[0].each do |id|
            TournamentEntrant.create!(team_id: id, tournament_id: tournament.id, competition_type: :group_a)
          end
          randomize_teams[1].each do |id|
            TournamentEntrant.create!(team_id: id, tournament_id: tournament.id, competition_type: :group_b)
          end

          Result.new(true, [], false)
      end
    rescue StandardError => e
      Result.new(false, [e], nil)
    end
  end
end