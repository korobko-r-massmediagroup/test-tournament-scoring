class MatchesGeneratorService < BaseService
  attr_reader :tournament

  def initialize(tournament)
    @tournament = tournament
  end

  def call
    %i[group_a group_b].each do |group|
      group_pair_teams = tournament.teams.joins(:tournament_entrants).where(tournament_entrants: { competition_type: group }).distinct

      group_pair_teams.each do |first_team|
        rejected_teams = group_pair_teams.where.not(id: first_team)
        rejected_teams.each do |second_team|
          unless Match.where(first_team: second_team, second_team: first_team).present?
            Match.create!(first_team: first_team.id, second_team: second_team.id, group: group, tournament_id: tournament.id)
          end
        end
      end
    end
  end
end