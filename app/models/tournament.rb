class Tournament < ApplicationRecord
  enum status: { draft: 0, in_progress: 1, done: 2 }

  has_many :matches
  has_many :tournament_entrants
  has_many :teams, through: :tournament_entrants

  validates :name, presence: true, uniqueness: true
end
