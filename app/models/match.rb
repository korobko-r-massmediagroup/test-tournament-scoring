class Match < ApplicationRecord
  enum group: %i[group_a group_b playoff]

  belongs_to :tournament
  belongs_to :winner, class_name: 'Team', optional: true
end
