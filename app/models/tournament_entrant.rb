class TournamentEntrant < ApplicationRecord
  enum competition_type: %i[group_a group_b playoff]

  belongs_to :team
  belongs_to :tournament
end