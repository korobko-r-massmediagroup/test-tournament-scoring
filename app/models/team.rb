class Team < ApplicationRecord
  has_many :tournament_entrants
  has_many :tournaments, through: :tournament_entrants

  validates :name, presence: true, uniqueness: true
end
