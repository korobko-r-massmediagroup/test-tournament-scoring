Rails.application.routes.draw do
  root to: "tournaments#index"

  resources :teams
  resources :tournaments do
    resources :tournament_entrants
    resources :matches do
      collection do
        get 'generate_results_for_all_matches'
      end
    end
    member do
      get 'generate_matches'
      get 'generate_playoff_matches'
    end
  end
end
